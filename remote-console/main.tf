provider "aws" {
  region     = "us-east-1"
  access_key = var.access_key
  secret_key = var.secret_key
}

data "aws_ami" "ubuntu" {
  owners      = ["099720109477"] # AWS account ID of Canonical
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }
}

resource "aws_key_pair" "deployer" {
  key_name   = "remote-console"
  public_key = file(var.public_key_file)
}

resource "aws_security_group" "allow_ssh" {
  name        = "remote_allow_ssh"
  description = "Allow SSH inbound traffic"
  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow_ssh"
  }
}

resource "aws_security_group" "allow_incoming_for_app" {
  name        = "remote_allow_ssh_incoming_for_app"
  description = "Allow incoming web traffic"
  ingress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow_ssh"
  }
}

resource "aws_security_group" "allow_ingress" {
  name        = "remote_allow_ingress"
  description = "Allow inbound traffic"
  ingress {
    protocol  = -1
    from_port = 0
    to_port   = 0
    self      = true
  }
  tags = {
    Name = "allow_ingress"
  }
}

resource "aws_security_group" "allow_outbound" {
  name        = "remote_allow_outbound"
  description = "Allow outbound traffic"
  egress {
    protocol    = -1
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "allow_outbound"
  }
}

resource "aws_instance" "master" {
  count         = 1
  ami           = data.aws_ami.ubuntu.id
  instance_type = "t2.medium"
  key_name      = aws_key_pair.deployer.key_name
  vpc_security_group_ids = [
    aws_security_group.allow_ssh.id,
    aws_security_group.allow_outbound.id,
    aws_security_group.allow_ingress.id,
    aws_security_group.allow_incoming_for_app.id
  ]

  tags = {
    Name = "RemoteConsole"
  }

  user_data = <<-EOF
  #!/bin/bash
  curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
  sudo apt-add-repository "deb [arch=$(dpkg --print-architecture)] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
  sudo apt install terraform -y
  sudo apt update
  sudo apt install software-properties-common
  sudo add-apt-repository --yes --update ppa:ansible/ansible
  sudo apt install ansible -y 
  sudo apt-get install -y apt-transport-https ca-certificates curl gnupg lsb-release
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
  echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
  sudo apt-get update
  sudo apt-get install -y docker-ce docker-ce-cli containerd.io
  sudo usermod -aG docker ubuntu
  EOF

}
