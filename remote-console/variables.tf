variable "access_key" {}
variable "secret_key" {}
variable "public_key_file" {
  type    = string
  default = "./remote-console.pub"
}
