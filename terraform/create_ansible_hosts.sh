cat > ../ansible/hosts <<EOF
[masters]
master ansible_host=$(terraform output --raw public_ip)
EOF
